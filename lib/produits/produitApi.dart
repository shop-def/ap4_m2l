import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ProduitApi {
  static const String baseUrl = "http://192.168.1.233:8000/api";

  static Future<String?> _getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  static Future<List<dynamic>> fetchProduitList() async {
    final token = await _getToken();
    final response = await http.get(
      Uri.parse('$baseUrl/produit/produits'),
      headers: {
        "Content-Type": "application/json",
        "authenticator": token!,
      },
    );
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load data: ${response.statusCode}');
    }
  }

  static Future<dynamic> fetchProduitById(String id) async {
    final token = await _getToken();
    final response = await http.get(
      Uri.parse('$baseUrl/produit/produit/$id'),
      headers: {
        "Content-Type": "application/json",
        "authenticator": token!,
      },
    );
    if (response.statusCode == 200) {
      final List<dynamic> data = jsonDecode(response.body);
      return data.isNotEmpty ? data[0] : null;
    } else {
      throw Exception('Failed to load product: ${response.statusCode}');
    }
  }

  static Future<void> editProduct(String id, String libelle, String reference,
      double prix, int stock, String imagePath) async {
    final token = await _getToken();
    final response = await http.put(
      Uri.parse('$baseUrl/admin/produit/update/$id'),
      body: jsonEncode({
        'libelle': libelle,
        'reference': reference,
        'prix': prix,
        'stock': stock,
        'image': imagePath,
      }),
      headers: {
        "Content-Type": "application/json",
        "authenticator": token!,
      },
    );

    if (response.statusCode == 200) {
      print("Produit mis à jour avec succès");
    } else {
      throw Exception(
          "Erreur lors de la mise à jour du produit: ${response.statusCode}");
    }
  }

  static Future<void> deleteProduct(String id) async {
    final token = await _getToken();
    final response = await http.put(
      Uri.parse('$baseUrl/admin/produit/delete/$id'),
      headers: {
        "Content-Type": "application/json",
        "authenticator": token!,
      },
    );

    if (response.statusCode == 200) {
      print("Produit supprimé avec succès");
    } else {
      throw Exception(
          "Erreur lors de la suppression du produit: ${response.statusCode}");
    }
  }

  static Future<void> addProduct(
      String libelle, String reference, double prix, int stock) async {
    final token = await _getToken();
    // Utiliser un chemin d'image prédéfini ou obtenu par une autre logique d'upload
    const String imagePath = 'mitigeur-lavabo.jpg';

    final response = await http.post(
      Uri.parse('$baseUrl/admin/produit/create'),
      headers: {
        "Content-Type": "application/json",
        "authenticator": token!,
      },
      body: jsonEncode({
        'libelle': libelle,
        'reference': reference,
        'prix': prix,
        'stock': stock,
        'image': imagePath,
      }),
    );

    if (response.statusCode == 201) {
      print("Produit ajouté avec succès");
    } else {
      throw Exception(
          "Erreur lors de l'ajout du produit: ${response.statusCode}");
    }
  }
}
