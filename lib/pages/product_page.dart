import 'package:flutter/material.dart';
import '../produits/produitApi.dart'; // Assurez-vous que le chemin est correct.

class ListProductsPage extends StatefulWidget {
  const ListProductsPage({super.key});

  @override
  _ListProductsPageState createState() => _ListProductsPageState();
}

class _ListProductsPageState extends State<ListProductsPage> {
  Future<List<dynamic>> fetchProductList() => ProduitApi.fetchProduitList();

  void _deleteProduct(String productId) async {
    try {
      await ProduitApi.deleteProduct(productId);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Produit supprimé avec succès')),
      );
      setState(() {}); // Trigger a rebuild to refresh the product list.
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('Erreur lors de la suppression du produit')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Liste des Produits'),
      ),
      body: FutureBuilder<List<dynamic>>(
        future:
            fetchProductList(), // Appel de la fonction pour rafraîchir la liste
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return const Center(
                child: Text(
                    'Une erreur est survenue lors du chargement des produits'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return const Center(child: Text('Aucun produit trouvé'));
          } else {
            return ListView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index) {
                final product = snapshot.data![index];
                return ListTile(
                  title: Text(product['libelle']),
                  subtitle: Text(
                      'Stock: ${product['stock']} - Prix: ${product['prix']} €'),
                  trailing: IconButton(
                    icon: const Icon(Icons.delete, color: Colors.red),
                    onPressed: () => _deleteProduct(product['numProduit']),
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
