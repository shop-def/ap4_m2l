import 'package:flutter/material.dart';
import '../produits/produitApi.dart'; // Assurez-vous que le chemin est correct pour votre structure de projet.

class EditProductPage extends StatefulWidget {
  const EditProductPage({Key? key}) : super(key: key);

  @override
  _EditProductPageState createState() => _EditProductPageState();
}

class _EditProductPageState extends State<EditProductPage> {
  List<dynamic> _products = [];
  String? _selectedProductId;
  bool _isLoading = true;
  bool _isFieldsVisible = false; // Contrôle la visibilité des champs

  final TextEditingController _labelController = TextEditingController();
  final TextEditingController _referenceController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _stockController = TextEditingController();
  String? _imagePath; // Pour stocker le chemin de l'image actuelle

  @override
  void initState() {
    super.initState();
    _fetchProducts();
  }

  Future<void> _fetchProducts() async {
    try {
      final products = await ProduitApi.fetchProduitList();
      setState(() {
        _products = products;
        _isLoading = false;
      });
    } catch (e) {
      print("Error fetching products: $e");
      setState(() => _isLoading = false);
    }
  }

  void _fetchAndDisplayProductDetails(String productId) async {
    try {
      final productDetails = await ProduitApi.fetchProduitById(productId);
      setState(() {
        _selectedProductId = productId;
        _labelController.text = productDetails['libelle'] ?? '';
        _referenceController.text = productDetails['reference'] ?? '';
        _priceController.text = productDetails['prix'].toString();
        _stockController.text = productDetails['stock'].toString();
        _imagePath = productDetails['image']; // Sauvegarde du chemin de l'image
        _isFieldsVisible = true;
      });
    } catch (e) {
      print("Error fetching product details: $e");
    }
  }

  Future<void> _modifyProduct() async {
    if (_selectedProductId != null && _imagePath != null) {
      final double prix = double.parse(_priceController.text);
      final int stock = int.parse(_stockController.text);
      ProduitApi.editProduct(
        _selectedProductId!,
        _labelController.text,
        _referenceController.text,
        prix,
        stock,
        _imagePath!, // Inclure le chemin de l'image conservé
      ).then((_) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Produit mis à jour avec succès')),
        );
      }).catchError((error) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content:
                  Text('Erreur lors de la mise à jour du produit: $error')),
        );
      });
    }
  }

  @override
  void dispose() {
    _labelController.dispose();
    _referenceController.dispose();
    _priceController.dispose();
    _stockController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Modifier un Produit'),
      ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: screenWidth * 0.9,
                    child: DropdownButtonFormField<String>(
                      isExpanded: true,
                      decoration: const InputDecoration(
                          labelText: "Sélectionner un produit",
                          labelStyle: TextStyle(color: Colors.black),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(254, 200, 22, 1),
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(254, 200, 22, 1)))),
                      value: _selectedProductId,
                      onChanged: (newValue) {
                        if (newValue != null) {
                          _fetchAndDisplayProductDetails(newValue);
                        } else {
                          setState(() {
                            _isFieldsVisible = false;
                          });
                        }
                      },
                      items: _products.map<DropdownMenuItem<String>>((product) {
                        return DropdownMenuItem<String>(
                          value: product['numProduit'],
                          child: Text(product['libelle']),
                        );
                      }).toList(),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Visibility(
                    visible: _isFieldsVisible,
                    child: Column(
                      children: [
                        TextField(
                          controller: _labelController,
                          decoration: const InputDecoration(
                            labelText: "Libellé",
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color.fromRGBO(254, 200, 22, 1),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 8),
                        TextField(
                          controller: _referenceController,
                          decoration: const InputDecoration(
                            labelText: "Référence",
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color.fromRGBO(254, 200, 22, 1),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 8),
                        TextField(
                          controller: _priceController,
                          keyboardType: const TextInputType.numberWithOptions(
                              decimal: true),
                          decoration: const InputDecoration(
                            labelText: "Prix",
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color.fromRGBO(254, 200, 22, 1),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 8),
                        TextField(
                          controller: _stockController,
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(
                            labelText: "Stock",
                            labelStyle: TextStyle(
                              color: Colors.black,
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Color.fromRGBO(254, 200, 22, 1),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 15),
                          child: ElevatedButton(
                            onPressed: _modifyProduct,
                            style: ElevatedButton.styleFrom(
                              foregroundColor: Colors
                                  .white, // S'assurer que le texte est visible
                              backgroundColor: const Color.fromRGBO(
                                  225, 33, 50, 1), // Couleur de fond
                            ),
                            // style: ElevatedButton.styleFrom( // Supprimez cette ligne
                            //   foregroundColor: Colors.white, // S'assurer que le texte est visible
                            //   backgroundColor: const Color.fromRGBO(225, 33, 50, 1), // Couleur de fond
                            // ),
                            child: const Text('Modifier le Produit'),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
