import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home_page.dart';
import 'pages/login_page.dart';
import 'pages/product_page.dart';
import 'pages/add_product_page.dart';
import 'pages/edit_product_page.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AP4 M2L',
      // La route initiale ou la page qui sera chargée au démarrage de l'app
      initialRoute: '/',
      // Définition des routes
      routes: {
        '/': (context) => FutureBuilder<bool>(
              future: _checkToken(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Scaffold(
                    body: Center(child: CircularProgressIndicator()),
                  );
                } else if (snapshot.hasData && snapshot.data!) {
                  return const MyHomePage(); // HomePage si l'utilisateur est authentifié
                } else {
                  return const LoginPage(); // LoginPage sinon
                }
              },
            ),
        '/home': (context) => const MyHomePage(),
        '/login': (context) => const LoginPage(),
        '/products': (context) => const ListProductsPage(),
        '/add_product': (context) => const AddProductPage(),
        '/edit_product': (context) => const EditProductPage(),
      },
    );
  }
}

Future<bool> _checkToken() async {
  final prefs = await SharedPreferences.getInstance();
  final token = prefs.getString('token');
  return token != null && token.isNotEmpty;
}
